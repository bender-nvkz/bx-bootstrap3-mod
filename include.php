<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

Loader::registerAutoLoadClasses('rekuz.bootstrap3', array(
    'Rekuz\Bootstrap3' => 'lib/Classes/Bootstrap3.php',
));


EventManager::getInstance()->addEventHandler(
    'main',
    'OnEndBufferContent',
    array('Rekuz\Bootstrap3', 'checkHeadStrOnEndBufferContent')
);
