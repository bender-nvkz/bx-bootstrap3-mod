<?php
namespace Rekuz;
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;
//use Bitrix\Main\Application\CBXVirtualIo;

Loc::loadMessages(__FILE__);

class Bootstrap3
{

    private static $_instance = null;

    private $BS_REL_PATH;
    private $BS_ABS_PATH;
    private $USE_LESS;
    private $BS_THEME;

    private $BXasset;

    protected function __clone() { }

    private function __construct() { }

    static private function getInstance() {
        if(is_null(self::$_instance))
        {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function init()
    {
        if(!$GLOBALS['bootstrap']) {
            $GLOBALS['bootstrap'] = '3.3.4';

            $bs = self::getInstance();

            $bs->BX_asset = \Bitrix\Main\Page\Asset::getInstance();
            $bs->USE_LESS = Option::get('rekuz.bootstrap3', "use_less");
            $bs->BS_THEME = Option::get('rekuz.bootstrap3', "bs_theme");

            if(!$bs->BS_ABS_PATH)
                $bs->BS_ABS_PATH = dirname(dirname(__FILE__))."/Assets/";
            if(!$bs->BS_REL_PATH)
                $bs->BS_REL_PATH = substr_replace(realpath(dirname(dirname(__FILE__))), '', 0, strlen($_SERVER['DOCUMENT_ROOT']))."/Assets/";
            $bs->addPageCSS();
            $bs->addPageJS();
            $bs->addIEcon();
        }
    }

    private function addPageCSS()
    {
        if($this->USE_LESS && \Bitrix\Main\Loader::includeModule('rekuz.lessphp')){
            Lessphp::addLess($this->BS_REL_PATH."less/bootstrap.less","bootstrap");
            if($this->BS_THEME)
                Lessphp::addLess($this->BS_REL_PATH."less/theme.less","bootstrap");
            return;
        }

        $this->BX_asset->addCss($this->BS_REL_PATH."dist/css/bootstrap.min.css");
        if($this->BS_THEME)
            $this->BX_asset->addCss($this->BS_REL_PATH."dist/css/bootstrap-theme.min.css");

        return;
    }

    public static function checkHeadStrOnEndBufferContent(&$content)
    {
        global $USER, $APPLICATION;
        if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
        if($APPLICATION->GetProperty("save_kernel") == "Y") return;
        $arPatternsToRemove = Array(
            '/<script.+?src=".+?kernel_main\/kernel_main\.js\?\d+"><\/script\>/',
            '/<script.+?src=".+?bitrix\/js[^"]+"><\/script\>/',
            '/<script.+?>BX\.(setCSSList|setJSList)\(\[.+?\]\).*?<\/script>/',
            '/<script.+?>if\(\!window\.BX\)window\.BX.+?<\/script>/',
            '/<script[^>]+?>\(window\.BX\|\|top\.BX\)\.message[^<]+<\/script>/',
            '/<script.+?>\nbxSession\.Expand\(.+?\).*?\n<\/script>/'
        );
        $content = preg_replace($arPatternsToRemove, "", $content);

        $arPatternsToRemove = Array(
            '/<link.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
            '/<link.+?href=".+?bitrix\/js\/main\/core\/css\/core[^"]+"[^>]+>/',
            /*
            '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/styles.css[^"]+"[^>]+>/',
            '/<link.+?href=".+?bitrix\/templates\/[\w\d_-]+\/template_styles.css[^"]+"[^>]+>/',
            */
        );

        $content = preg_replace($arPatternsToRemove, "", $content);

        $content = preg_replace("/\n{2,}/", "\n\n", $content);
    }

    private function addPageJS()
    {
        $this->BX_asset->addJs($this->BS_REL_PATH."dist/js/jquery.min.js");
        $this->BX_asset->addJs($this->BS_REL_PATH."dist/js/bootstrap.min.js");
        $this->BX_asset->addJs($this->BS_REL_PATH."dist/js/ie10-viewport-bug-workaround.js");
        return;
    }

    private function addIEcon()
    {
        $ifIE = "<!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->";
        $this->BX_asset->addString($ifIE,true,'AFTER_CSS');
        return;
    }
}
